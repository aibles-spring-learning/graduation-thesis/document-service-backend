package org.aibles.comment.dto.req;

import lombok.Data;

@Data
public class CommentReqDto {
    private String content;
}
