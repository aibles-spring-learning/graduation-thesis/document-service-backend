package org.aibles.comment.repository;

import org.aibles.comment.model.Comment;
import org.aibles.document.model.Document;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {
    Page<Comment> findByDocument_IdOrderByCommentTimeDesc(final long documentId, final Pageable pageable);
}
