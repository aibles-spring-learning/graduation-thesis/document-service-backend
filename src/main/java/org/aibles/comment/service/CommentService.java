package org.aibles.comment.service;

import org.aibles.comment.dto.req.CommentReqDto;
import org.aibles.comment.dto.res.CommentResDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface CommentService {
    CommentResDto createComment(final long documentId, final CommentReqDto commentReqDto);
    Page<CommentResDto> listDocumentComments(final Long documentId, final Pageable pageable);
}
