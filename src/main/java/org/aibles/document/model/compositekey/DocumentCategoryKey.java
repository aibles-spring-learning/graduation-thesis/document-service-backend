package org.aibles.document.model.compositekey;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DocumentCategoryKey implements Serializable {

    @Column(name = "document_id")
    private long documentId;

    @Column(name = "category_id")
    private long categoryId;

    public DocumentCategoryKey(long categoryId) {
        this.categoryId = categoryId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DocumentCategoryKey)) return false;
        DocumentCategoryKey that = (DocumentCategoryKey) o;
        return getDocumentId() == that.getDocumentId() && getCategoryId() == that.getCategoryId();
    }

    @Override
    public int hashCode() {
        return 31;
    }
}
