package org.aibles.document.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.aibles.user.model.User;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "view_document")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ViewDocument implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "view_document_id_seq")
    @SequenceGenerator(name = "view_document_id_seq",
        sequenceName = "pk_view_document_id_seq",
        allocationSize = 1)
    @Column(name = "id")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "document_id")
    private Document document;
}
