package org.aibles.document.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.aibles.document.model.compositekey.DocumentCategoryKey;

import javax.persistence.*;

@Entity
@Table(name = "document_category")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DocumentCategory {

    @EmbeddedId
    DocumentCategoryKey id;

    @ManyToOne(fetch = FetchType.EAGER)
    @MapsId("categoryId")
    @JoinColumn(name = "category_id")
    private Category category;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("documentId")
    @JoinColumn(name = "document_id")
    private Document document;

    public DocumentCategory(DocumentCategoryKey id, Category category) {
        this.id = id;
        this.category = category;
    }
}
