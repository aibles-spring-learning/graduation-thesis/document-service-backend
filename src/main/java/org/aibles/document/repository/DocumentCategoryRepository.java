package org.aibles.document.repository;

import org.aibles.document.model.DocumentCategory;
import org.aibles.document.model.compositekey.DocumentCategoryKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface DocumentCategoryRepository extends JpaRepository<DocumentCategory, DocumentCategoryKey> {

    @Modifying
    @Query(value = "DELETE FROM document_category  " +
            "WHERE document_id = :documentId AND category_id not in :categoryIds",
    nativeQuery = true)
    void clear(@Param("documentId") long documentId,@Param("categoryIds") Set<Long> categoriesId);
}
