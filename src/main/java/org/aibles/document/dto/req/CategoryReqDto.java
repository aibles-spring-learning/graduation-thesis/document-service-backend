package org.aibles.document.dto.req;

import lombok.Data;

import javax.validation.constraints.NotBlank;


@Data
public class CategoryReqDto {

    @NotBlank(message = "Category's name can not be blank")
    private String name;
}
