package org.aibles.document.dto.req;

import lombok.Data;
import org.aibles.document.model.constants.DocumentType;

import javax.validation.constraints.*;
import java.util.Set;

@Data
public class DocumentReqDto {
    @NotBlank(message = "Title can not be null")
    private String title;

    private String description;

    private String author;

    @NotBlank(message = "File s3 object key can not be null")
    private String fileS3ObjectKey;

    private String thumbS3ObjectKey;

    @NotNull
    private DocumentType type;

    private Set<Long> categoryIds;
}
