package org.aibles.document.service;

import org.aibles.document.dto.req.CategoryReqDto;
import org.aibles.document.dto.res.CategoryResDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


public interface CategoryService {
    CategoryResDto createCategory(final CategoryReqDto categoryReqDto);

    Page<CategoryResDto> getCategories(Pageable pageable);
}
