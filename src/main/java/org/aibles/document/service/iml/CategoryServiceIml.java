package org.aibles.document.service.iml;

import lombok.RequiredArgsConstructor;
import org.aibles.document.dto.req.CategoryReqDto;
import org.aibles.document.dto.res.CategoryResDto;
import org.aibles.document.model.Category;
import org.aibles.document.repository.CategoryRepository;
import org.aibles.document.service.CategoryService;
import org.aibles.util.component.MappingHelper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class CategoryServiceIml implements CategoryService {

    private final CategoryRepository categoryRepository;
    private final MappingHelper mappingHelper;


    @Override
    public CategoryResDto createCategory(CategoryReqDto categoryReqDto) {
        Category category = mappingHelper.map(categoryReqDto, Category.class);
        System.out.println(category.toString());
        category = categoryRepository.save(category);
        return mappingHelper.map(category, CategoryResDto.class);
    }

    @Override
    public Page<CategoryResDto> getCategories(Pageable pageable) {
        Page<Category> categoryPage = categoryRepository.findAll(pageable);
        categoryPage.getPageable().getPageNumber();
        return mappingHelper.mapPage(categoryPage, CategoryResDto.class);
    }
}
