package org.aibles.document.service.iml;

import lombok.RequiredArgsConstructor;
import org.aibles.authentication.SecurityContextManager;
import org.aibles.document.dto.req.DocumentReqDto;
import org.aibles.document.dto.res.BasicDocumentResDto;
import org.aibles.document.dto.res.DetailDocumentResDto;
import org.aibles.document.model.Category;
import org.aibles.document.model.Document;
import org.aibles.document.model.DocumentCategory;
import org.aibles.document.model.ViewDocument;
import org.aibles.document.model.compositekey.DocumentCategoryKey;
import org.aibles.document.repository.CategoryRepository;
import org.aibles.document.repository.DocumentCategoryRepository;
import org.aibles.document.repository.DocumentRepository;
import org.aibles.document.repository.ViewDocumentRepository;
import org.aibles.document.service.DocumentService;
import org.aibles.user.model.ReadHistory;
import org.aibles.user.model.User;
import org.aibles.user.repository.ReadHistoryRepository;
import org.aibles.user.repository.UserRepository;
import org.aibles.util.component.MappingHelper;
import org.aibles.util.exceptions.DocumentNotFoundException;
import org.aibles.util.paging.PagingRes;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Clock;
import java.time.Instant;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class DocumentServiceIml implements DocumentService {

    private final DocumentRepository documentRepository;
    private final CategoryRepository categoryRepository;
    private final DocumentCategoryRepository documentCategoryRepository;
    private final ViewDocumentRepository viewDocumentRepository;
    private final MappingHelper mappingHelper;
    private final SecurityContextManager securityContextManager;
    private final UserRepository userRepository;
    private final ReadHistoryRepository readHistoryRepository;

    @Override
    public DetailDocumentResDto createDocument(DocumentReqDto documentReqDto) {
        Document document = mappingHelper.map(documentReqDto, Document.class);
        document.setPublicationDate(Instant.now(Clock.systemDefaultZone()));
        document.setDocumentCategories(this.buildSetDocumentCategories(documentReqDto.getCategoryIds()));
        return mappingHelper.map(documentRepository.save(document), DetailDocumentResDto.class);
    }

    @Override
    @Transactional
    public DetailDocumentResDto updateDocument(long documentId, DocumentReqDto documentReqDto) {
        System.out.println(documentReqDto.getThumbS3ObjectKey());
        System.out.println(documentReqDto.getDescription());
        Document document = documentRepository.findById(documentId)
                .map(doc -> {
                    doc.setAuthor(documentReqDto.getAuthor());
                    doc.setDescription(documentReqDto.getDescription());
                    doc.setFileS3ObjectKey(documentReqDto.getFileS3ObjectKey());
                    doc.setThumbS3ObjectKey(documentReqDto.getThumbS3ObjectKey());
                    doc.setTitle(documentReqDto.getTitle());
                    doc.setType(documentReqDto.getType());
                    return doc;
                })
                .orElseThrow(DocumentNotFoundException::new);

        deleteDocumentCategories(documentId, documentReqDto.getCategoryIds());

        Set<DocumentCategory> documentCategories = this.buildSetDocumentCategories(documentReqDto.getCategoryIds());
        documentCategories.stream()
                        .map(dc -> {
                            dc.getId().setDocumentId(documentId);
                            dc.setDocument(document);
                            return dc;
                        }).collect(Collectors.toSet());
        document.setDocumentCategories(documentCategories);
        return mappingHelper.map(documentRepository.save(document), DetailDocumentResDto.class);
    }

    @Override
    public Page<BasicDocumentResDto> listDocuments(Pageable pageable) {
        final Page<Document> documentPage = documentRepository.findAllByOrderByPublicationDateDesc(pageable);
        return mappingHelper.mapPage(documentPage, BasicDocumentResDto.class);
    }

    @Override
    public DetailDocumentResDto deleteDocument(long documentId) {
        return documentRepository.findById(documentId)
                .map(document -> {
                    documentRepository.delete(document);
                    return mappingHelper.map(document, DetailDocumentResDto.class);
                })
                .orElseThrow(DocumentNotFoundException::new);
    }

    @Override
    public Page<BasicDocumentResDto> listDocumentsByCategory(long categoryId, final Pageable pageable) {
        Page<Document> documentPage = documentRepository.findByDocumentCategoriesCategoryId(categoryId, pageable);
        return mappingHelper.mapPage(documentPage, BasicDocumentResDto.class);
    }

    @Override
    public DetailDocumentResDto getDocument(long documentId) {
        final Document document = documentRepository.findById(documentId)
                .orElseThrow(DocumentNotFoundException::new);
        DetailDocumentResDto documentResDto = mappingHelper.map(document, DetailDocumentResDto.class);
        documentResDto.setViews(viewDocumentRepository.countByDocumentId(document.getId()));

        final User user = securityContextManager.getCurrentLoginUser();
        if (!userRepository.existsById(user.getId())){
            userRepository.save(user);
        }

        updateViewDocument(document, user);

        updateReadHistory(document, user);

        return documentResDto;
    }

    @Override
    public Page<BasicDocumentResDto> listDocumentsByTitle(String title, Pageable pageable) {
        final Page<Document> documentPage = documentRepository.findByTitleContainingOrderByPublicationDateDesc(title, pageable);
        return mappingHelper.mapPage(documentPage, BasicDocumentResDto.class);
    }

    private Set<DocumentCategory> buildSetDocumentCategories(Set<Long> categoryIds){
        final Set<Category> categories = categoryRepository.findByIdIn(categoryIds);
        final Set<DocumentCategory> documentCategories = categories.stream()
                .map(category -> {
                    DocumentCategoryKey documentCategoryKey = new DocumentCategoryKey(category.getId());
                    DocumentCategory documentCategory = new DocumentCategory(documentCategoryKey, category);
                    return documentCategory;
                })
                .collect(Collectors.toSet());
        return documentCategories;
    }

    private void deleteDocumentCategories(final long documentId, final Set<Long> categoriesId){
        documentCategoryRepository.clear(documentId, categoriesId);
    }

    @Async
    public void updateViewDocument(final Document document, final User user){
        ViewDocument viewDocument = new ViewDocument();
        viewDocument.setDocument(document);
        viewDocument.setUser(user);

        viewDocumentRepository.save(viewDocument);
    }

    @Async
    public void updateReadHistory(final Document document, final User user){
        ReadHistory readHistory = new ReadHistory();
        readHistory.setDocument(document);
        readHistory.setUser(user);

        readHistoryRepository.save(readHistory);
    }
}
