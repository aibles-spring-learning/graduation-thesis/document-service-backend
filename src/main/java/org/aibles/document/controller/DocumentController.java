package org.aibles.document.controller;

import lombok.RequiredArgsConstructor;
import org.aibles.document.dto.req.DocumentReqDto;
import org.aibles.document.dto.res.BasicDocumentResDto;
import org.aibles.document.dto.res.DetailDocumentResDto;
import org.aibles.document.service.DocumentService;
import org.aibles.util.paging.PagingReq;
import org.aibles.util.paging.PagingRes;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/documents")
@RequiredArgsConstructor
public class DocumentController {

    private final DocumentService documentService;

    @PostMapping
    public ResponseEntity<DetailDocumentResDto> createDocument(@Validated() @RequestBody() final DocumentReqDto documentReqDto){
        final DetailDocumentResDto documentResDto = documentService.createDocument(documentReqDto);
        return ResponseEntity.status(HttpStatus.CREATED).body(documentResDto);
    }

    @PutMapping("/{id}")
    public ResponseEntity<DetailDocumentResDto> updateDocument(@PathVariable("id") final long id,
                                                               @Validated() @RequestBody() final DocumentReqDto documentReqDto){
        final DetailDocumentResDto documentResDto = documentService.updateDocument(id, documentReqDto);
        return ResponseEntity.ok(documentResDto);
    }

    @GetMapping
    public ResponseEntity<PagingRes<BasicDocumentResDto>> listDocuments(@Validated() final PagingReq pagingReq){
        final Page<BasicDocumentResDto> documentResDtoPage = documentService.listDocuments(pagingReq.makePageable());
        return ResponseEntity.ok(PagingRes.of(documentResDtoPage));
    }

    @GetMapping(params = {"categoryId"})
    public ResponseEntity<PagingRes<BasicDocumentResDto>> listDocumentsByCategory(@RequestParam("categoryId") final long categoryId,
                                                                             @Validated() final PagingReq pagingReq){
        final Page<BasicDocumentResDto> documentResDtoPage = documentService.listDocumentsByCategory(categoryId, pagingReq.makePageable());
        return ResponseEntity.ok(PagingRes.of(documentResDtoPage));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<DetailDocumentResDto> deleteDocument(@PathVariable("id") final long id){
        final DetailDocumentResDto documentResDto = documentService.deleteDocument(id);
        return ResponseEntity.ok(documentResDto);
    }

    @GetMapping("/{id}")
    public ResponseEntity<DetailDocumentResDto> getDocument(@PathVariable("id") final long id){
        final DetailDocumentResDto documentResDto = documentService.getDocument(id);
        return ResponseEntity.ok(documentResDto);
    }

    @GetMapping(params = {"title"})
    public ResponseEntity<PagingRes<BasicDocumentResDto>> listDocumentsByTitle(@RequestParam("title") String title,
                                                                               @Validated() PagingReq pagingReq){
        final Page<BasicDocumentResDto> documentPage = documentService.listDocumentsByTitle(title, pagingReq.makePageable());
        return ResponseEntity.ok(PagingRes.of(documentPage));
    }
}
