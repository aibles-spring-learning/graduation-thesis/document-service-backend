package org.aibles.authentication;

import lombok.RequiredArgsConstructor;
import org.aibles.user.model.User;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.Map;

@Component
@RequiredArgsConstructor
public class SecurityContextManager {

    private final HttpServletRequest httpServletRequest;

    public User getCurrentLoginUser(){
        final Principal principal = httpServletRequest.getUserPrincipal();
        final JwtAuthenticationToken jwtAuthenticationToken = (JwtAuthenticationToken) principal;
        final Map<String, Object> tokenAttributes = jwtAuthenticationToken.getTokenAttributes();

        User user = new User();
        user.setId(tokenAttributes.get("sub").toString());
        user.setEmail(tokenAttributes.get("email").toString());
        user.setName(tokenAttributes.get("name").toString());
        return user;
    }
}
