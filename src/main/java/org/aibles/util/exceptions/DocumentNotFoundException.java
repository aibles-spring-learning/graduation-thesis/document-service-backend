package org.aibles.util.exceptions;

import org.aibles.util.exceptions.common.BusinessException;
import org.aibles.util.exceptions.common.ServiceError;

public class DocumentNotFoundException extends BusinessException {
    public DocumentNotFoundException() {
        super(ServiceError.DOCUMENT_NOT_FOUND_EXCEPTION);
    }
}
