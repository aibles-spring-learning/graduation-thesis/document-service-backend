package org.aibles.util.exceptions.common;

import java.util.LinkedHashMap;

public class BusinessException extends ServiceException{

    private static final long serialVersionUID = 6910434576449212427L;

    protected BusinessException(ServiceError err, Throwable ex, LinkedHashMap<String, Object> params) {
        super(err, ex, params);
    }

    public BusinessException(ServiceError err){
        super(err, null ,null);
    }

    public BusinessException(ServiceError err, Throwable ex){
        super(err, ex ,null);
    }
}
