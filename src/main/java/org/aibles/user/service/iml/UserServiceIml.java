package org.aibles.user.service.iml;

import lombok.RequiredArgsConstructor;
import org.aibles.authentication.SecurityContextManager;
import org.aibles.document.dto.res.BasicDocumentResDto;
import org.aibles.document.model.Document;
import org.aibles.document.repository.DocumentRepository;
import org.aibles.user.model.User;
import org.aibles.user.service.UserService;
import org.aibles.util.component.MappingHelper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserServiceIml implements UserService {

    private final DocumentRepository documentRepository;
    private final MappingHelper mappingHelper;
    private final SecurityContextManager securityContextManager;

    @Override
    public Page<BasicDocumentResDto> listReadingHistory(Pageable pageable) {
        final User user = securityContextManager.getCurrentLoginUser();
        final Page<Document> documentPage = documentRepository.findByReadHistoriesUserId(user.getId(), pageable);
        return mappingHelper.mapPage(documentPage, BasicDocumentResDto.class);
    }
}
