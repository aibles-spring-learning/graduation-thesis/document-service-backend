package org.aibles.user.service;

import org.aibles.document.dto.res.BasicDocumentResDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface UserService {
    Page<BasicDocumentResDto> listReadingHistory(final Pageable pageable);
}
