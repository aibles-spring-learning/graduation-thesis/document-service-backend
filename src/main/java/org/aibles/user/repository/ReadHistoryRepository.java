package org.aibles.user.repository;

import org.aibles.user.model.ReadHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReadHistoryRepository extends JpaRepository<ReadHistory, Long> {
}
