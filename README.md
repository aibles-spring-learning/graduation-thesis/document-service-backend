# Leonard's graduation thesis: Aibles's document service

This project is the backend server for my application.

## Installation

Before installation the project, you need copy file sample.evn to .env file and fill out your information
```env
DATABASE_HOST=
DATABASE_PORT=
DATABASE_USER=
DATABASE_PASSWORD=
DATABASE_NAME=

S3_ACCESS_KEY=
S3_SECRET_KEY=
S3_BUCKET=

KEYCLOAK_URL=
KEYCLOAK_REALM=
```
If you don't use Amazon s3, you can ignore it. And disable all things in the code that relate to s3. It is at package: /src/main/java/org/aibles/awss3.

If you don't know how to use keycloak. You can disable it by disable auth.\
How to disable auth? You can change code in the class /src/main/java/org/aibles/authentication/SecurityConfig.java
```java
@Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests(authorizeRequests -> authorizeRequests
                        .antMatchers("/api**").permitAll()
                        .anyRequest().authenticated())
                .oauth2ResourceServer(OAuth2ResourceServerConfigurer::jwt);
    }


```


Then, you can run it by click run botton in your IDE. PLease make sure you configured .env file for your IDE.


## How to test the api
you can see all api in my postman document in the postman link: https://documenter.getpostman.com/view/9313701/UVJbGcy5

## Contributor
My Vietnammese name: Trịnh Văn Đạt
My English name: Leonard
My birth day: 09/03/1999

## References
[How to use presigned-url of Amazon S3 storage](https://docs.aws.amazon.com/AmazonS3/latest/userguide/PresignedUrlUploadObject.html)\
[How to use the keycloak as the authorization server](https://www.keycloak.org/documentation)\
[How to design Restful API](https://cloud.google.com/apis/design)\
[How to use Spring boot framework](https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle)\
[How to use Docker & Docker-compose](https://docs.docker.com/get-started/overview/)
