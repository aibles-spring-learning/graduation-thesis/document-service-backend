#
# Build stage
#
FROM maven:3.6.0-jdk-11-slim AS build

# copy the project files
COPY ./pom.xml ./pom.xml

# build all dependencies for offline use
RUN mvn dependency:go-offline -B

COPY src /src
RUN mvn clean package -Dmaven.test.skip

#
# Package stage
#
FROM openjdk:11-jre-slim
COPY --from=build /target/document-service-0.0.1-SNAPSHOT.jar /usr/local/lib/document-service.jar
EXPOSE 8099
ENTRYPOINT ["java","-jar","/usr/local/lib/document-service.jar"]